sshd
=========

This module ensures that the sshd service is installed. It configures the sshd server with a default sshd_config, which is modified by some parameters

Requirements
------------

None

Role Variables
--------------

enable_sshd: true - [controls if the module will run at all]

sshd_service_state: "started" - [starts the service if enabled]

sshd_permitrootlogin: "without-password" - [specifies whether root can log in using ssh]

sshd_maxauthtries: "3" - [specifies the maximum number of authentication attempts permitted per connection]

sshd_x11forwarding: "no" - [specifies whether X11 forwarding is permitted]

sshd_ssh_port: "22" - [specifies the port number that sshd listens on]

sshd_addressfamily: "any" - [specifies which address family should be used by sshd]

sshd_x11displayoffset: "10" - [specifies the first display number available for sshd X11 forwarding]

sshd_allowtcpforwarding: "no" - [specifies whether TCP forwarding is permitted]

sshd_x11uselocalhost: "yes" - [specifies whether sshd should bind the X11 forwarding server to the loopback address or to the wildcard address]

sshd_pubkeyauthentication: "yes" - [specifies whether public key authentication is allowed]

sshd_maxsessions: "10" - [specifies the maximum number of open shell, login or subsystem (e.g. sftp) sessions permitted per network connection]

sshd_authorizedkeyscommand: "" - [specifies a program to be used to look up the user's public keys. If no arguments are specified then the username of the target user is used]

sshd_authorizedkeyscommanduser: "" - [specifies the user under whose account the AuthorizedKeysCommand is run]

sshd_maxstartups: "10:30:100" - [specifies the maximum number of concurrent unauthenticated connections to the SSH daemon]

sshd_ciphers: "aes128-ctr,aes192-ctr,aes256-ctr" - [specifies the ciphers allowed. Multiple ciphers must be comma-separated]

sshd_macs: "hmac-sha1-etm@openssh.com,umac-64-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-ripemd160-etm@openssh.com,hmac-sha1,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,hmac-ripemd160" - [specifies the available MAC (message authentication code) algorithms]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
